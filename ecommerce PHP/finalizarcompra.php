<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");	
		
		
		include_once("conectar.php");
		
		$pagto = $_POST['pagto'];//pega a opção da radio
		
		//criando as variáveis vazias
		$numero = "";
		$nomecartao = "";
		$mes = "";
		$ano = "";
		$digito = "";
		$vezes = "";
		$data = date('Y-m-d H:i');
		$status = "aguardando";
		
		$total = $_SESSION['total'];
		$codcli = $_SESSION['codcli_session'];
		
		
		if($pagto != "boleto")
		{
			//pegar valor dos campos
			$numero = $_POST['numero'];
			$nomecartao = $_POST['nome'];
		    $mes = $_POST['mes'];
			$ano = $_POST['ano'];
			$digito = $_POST['digito'];
			$vezes = $_POST['vezes'];
			
		}
		//primeiro vamos cadastrar o pedido
		$cadastrar = mysql_query("insert into pedido (codcli,data,total,formapagto,ncartao,digito,mes,ano,vezes,status) values ('$codcli','$data','$total','$pagto','$numero','$digito','$mes','$ano','$vezes','$status')") or die (mysql_error());
		$codpedido = mysql_insert_id();//retorna o último código cadastrar, ou seja aquele que acabou de ser cadastrado

		if($cadastrar)//se conseguiu cadastrar
		{
			//cadastrando os itens
			foreach($_SESSION['carrinho'] as $prod => $codigo)//rodando todos os itens do vetor
			{
				$cor = $_SESSION['cor'][$codigo];
				$tamanho = $_SESSION['tamanho'][$codigo];
				$qtd = $_SESSION['quantidade'][$codigo];
				
				$cadastraritem = mysql_query("insert into itens (codroupa,codpedido,qtdparcial,tamanho,cor) values ('$codigo','$codpedido','$qtd','$tamanho','$cor')") or die (mysql_error());				
			}
		}
		//limpar dados da sessão
		unset($_SESSION['carrinho']);
		unset($_SESSION['quantidade']);
		unset($_SESSION['cor']);
		unset($_SESSION['tamanho']);
		unset($_SESSION['total']);
		
		
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	        <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
</ul>
      <div class="contact">
      	<div class="container">
      	   <div class="contact_top">
      		<div>
      			<div class="contact_grid contact_address">
					<h3>PARABÉNS PELA SUA COMPRA!</h3>
					<h2><strong>ANOTE O NÚMERO DO SEU PEDIDO: <?php echo $codpedido;?></strong></h2>
					<p><a href="index.php" class="btn1 btn-primary1">Finalizar e Sair</a>

<?php 

if($pagto == "boleto")
{
	?>
					<a href="boletobb.php?total=<?php echo $total;?>" target="_blank" class="btn1 btn-primary1">Imprimir Boleto</a></p>
					<?php
}?>
					
				</div>
      		</div>
      		
      		<div class="clearfix"> </div>
      	   </div>      		
      	</div>
      </div>
	  <?php
      include_once("rodape.php");
	  ?>
</body>
</html>		