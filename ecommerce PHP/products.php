﻿<?php 
include_once("conectar.php");

$codcateg = "";
$tipo = "";
$order = "nome";
if(!empty($_GET["tipo"]))
	$tipo = $_GET["tipo"];
if(!empty($_GET["codcateg"]))
	$codcateg = $_GET["codcateg"];
if(!empty($_POST["order"]))
	$order = $_POST["order"];
?>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
		    <script type="text/javascript">
			    $(document).ready(function () {
			        $('#horizontalTab').easyResponsiveTabs({
			            type: 'default', //Types: default, vertical, accordion           
			            width: 'auto', //auto or any width like 600px
			            fit: true   // 100% fit in a container
			        });
			    });
</script>	
</head>
<body>
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	         <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
 <div class="container">
  </div>
</ul>

     <div class="products_top">
     	<div class="container">
     		<div class="col-md-9">
     			<div class="mens-toolbar">
                 <div class="sort">
				 <form action = "products.php?tipo=<?php echo $tipo;?>&codcateg=<?php echo $codcateg;?>" method="post">
               	   <div class="sort-by">
		            <label>Ordenar por</label>
		            <select name="order">
					<option value="<?php echo $order;?>">
					<?php
					$ordenado= "";
					if($order == "nome")
					 $ordenado = "Nome";
				 else if($order == "menor")
					 $ordenado = "Menor Preço";
				 else if($order == "maior")
					 $ordenado = "Maior Preço";
					
					echo $ordenado;
					?>
					
					</option>					
							<option value="nome">Nome</option>
							<option value="menor">Menor Preço</option>
							<option value="maior">Maior Preço</option>
		            </select>
		            <input type="submit" value="Ok"/>					
					</form>
                   </div>
    		     </div>
    		   
                <div class="clearfix"></div>		
		        </div>
			  <div class="product_box1">   
		        <ul class="grid-4">
				
				 <?php
				 $comando = "";
				 if($order == "nome")
					 $order = "titulo";
				 else if($order == "menor")
					 $order = "valor";
				 else if($order == "maior")
					 $order = "valor desc";
				
				 
				 if($codcateg != "")
					$comando = "select * from roupa where tipo = '$tipo' and codcateg = '$codcateg' order by $order";
				else
					$comando = "select * from roupa where tipo = '$tipo' order by $order";
				
				$buscarroupas = mysql_query($comando) or die (mysql_error());
				
				//comando paginação
				
					$pagina = 1;//página atual, inicia na primeira
					//pegando a quantidade caso ele clique em alguma página
					if(!empty($_GET["pagina"]))
					{
						$pagina = $_GET["pagina"];
					}

					$total_registros = mysql_num_rows($buscarroupas);//conta o total de registros

					$itens_pagina = 6; //qauntidade de itens por página

					//calculando o número de páginas
					$numero_paginas = ceil($total_registros / $itens_pagina); //comando para arredondar para cima (CEIL)

					//calculando o início da visualização com base na página escolhida
					$inicio = ($itens_pagina*$pagina) - $itens_pagina;

					//consultando de acordo com o a página escolhida. Atualiza a consulta para mostrar de acordo com inicio e fim
					$comando .= " limit $inicio, $itens_pagina";
					$buscarroupas =  mysql_query($comando) or die (mysql_error());		
				
				
			 
					
					$controupa = 1;
					$totalroupas = mysql_num_rows($buscarroupas);
					
					while($dadosroupas = mysql_fetch_assoc($buscarroupas))
					{
						$codroupa = $dadosroupas["codroupa"];
						$titulo = $dadosroupas["titulo"];
						$valor = number_format($dadosroupas["valor"],2,",",".");
						$imagem1 = $dadosroupas["foto1"];
						$classe = "";
						if($controupa == $totalroupas)
							$classe = "grid-4_last";
							
					?>
				
          		  <li class="<?php echo $classe;?>"><a href="single.php?codroupa=<?php echo $codroupa;?>">
					<div class="view view-first">
					  <img src="adm/pages/<?php echo $imagem1;?>" class="img-responsive" alt=""/>
					   <div class="product_mask"> </div>
					   <div class="tab_desc1">
			             <h3><a href="#"><?php  echo "$titulo"; ?></a></h3>
						 <p>R$ <?php echo $valor;?></p>
						 <a href="single.php?codroupa=<?php echo $codroupa;?>" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""></a>
					   </div>
					 </a></div>
				  </li>				  		  
					<?php $controupa++;} ?>
				  <div class="clearfix"> </div>	
				  
				</ul>
			  </div>
			  <div class="pages">   
        	     <div class="limiter visible-desktop">
	               	<label>Páginas</label>					
	             </div>
				 
       	         <ul class="dc_pagination dc_paginationA dc_paginationA06">
				 <?php
				//exibe a páginação
				for($i=1;$i <= $numero_paginas;$i++)
				{
				?>
					<li><a href="products.php?tipo=<?php echo $tipo;?>&codcateg=<?php echo $codcateg;?>&pagina=<?php echo $i;?>"><?php echo $i;?></a></li>					
					
				<?php
				}
				
				if($pagina < $numero_paginas)
				{
					?>
				
					<li><a href="products.php?tipo=<?php echo $tipo;?>&codcateg=<?php echo $codcateg;?>&pagina=<?php
   	
		$proxima = $pagina + 1;
		echo "$proxima";
	
				?>" class="current"></a></li><?php } ?>
		       	  </ul>
		  	      <div class="clearfix"></div>
		  	   </div>
     		</div>
     		<div class="col-md-3 product_right">
     			<h3 class="m_1">Categorias</h3>
				
				 <ul class="grid_1">					
					<?php
					$buscarcateg = mysql_query("select * from categoria order by nomecateg") or die (mysql_error());
					while($dadoscateg = mysql_fetch_assoc($buscarcateg))
					{
						$codcateg = $dadoscateg["codcateg"];
						$nomecateg = $dadoscateg["nomecateg"];
					?>
					
					<li class="grid_1-desc">
					  <h4><a href="products.php?codcateg=<?php echo $codcateg;?>&tipo=<?php echo $tipo;?>"><?php echo $nomecateg;?></a></h4>
					</li>
					<?php 
					}
					?>
					<div class="clearfix"> </div>					
					
				  </ul>					  
     			    
			    <ul class="size">
				</ul>  
				<h3 class="m_1">Destaques</h3>
				 <div class="sale_grid">
				 <?php
								 
					$buscardestaque = mysql_query("select * from roupa order by rand() limit 5 ") or die (mysql_error());
					while($dadosdestaque = mysql_fetch_assoc($buscardestaque))
					{
						$codroupa = $dadosdestaque["codroupa"];
						$titulo = $dadosdestaque["titulo"];
						$valor = number_format($dadosdestaque["valor"],2,",",".");
						$imagem1 = $dadosdestaque["foto1"];
					?>
				 
				  <ul class="grid_1">
					<li class="grid_1-img"><img src="<?php echo 'adm/pages/'.$imagem1;?>" class="img-responsive" alt=""/></li>
					<li class="grid_1-desc">
					  <h4><a href="single.php?codroupa=<?php echo $codroupa;?>"><?php echo $titulo;?></a></h4>
					  <p>R$ <?php echo $valor;?></p>
					</li>
					<div class="clearfix"> </div>
				  </ul>	
				  <?php 
					}
					?>
				</div>
			  
     		</div>
     	</div>
      </div>
      <div class="grid-2">
       	<?php
      include_once("rodape.php");
	  ?>
</body>
</html>	
<?php 
mysql_close($conectar);
?>	