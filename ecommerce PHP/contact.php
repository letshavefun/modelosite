﻿<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	        <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
</ul>
      <div class="contact">
      	<div class="container">
      	   <div class="contact_top">
      		<div class="col-md-3 contact_left">
      			<div class="contact_grid contact_address">
					<h3>Endereço</h3>	
					<p>Rua Travessa do parque 6, n. 74</p>
					<p>07990-095 - Belém Capela</p>
					<p>Francisco Morato / SP</p>
					<p>Telefone(11)4608-2816</p>					
					<p>Email: anderson.siqueira14@etec.sp.gov.br</a></p>
				</div>
      		</div>
      		<div class="col-md-9">
      			<div class="contact-form">
				   <form method="post" action="recebecontato.php">
					  <input name="txtnome" type="text" class="textbox" id="txtnome" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}" value="Nome">
					  <input name="txtemail" type="text" class="textbox" id="txtemail" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" value="Email">
					  <textarea name="txtarea" id="txtarea" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Mensagem';}" value="Mensagem:">Mensagem</textarea>
					  <input name="button" type="submit" id="button" value="Enviar">
				   </form>
				</div>
      		</div>
      		<div class="clearfix"> </div>
      	   </div>
      		<div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d647.8771239413712!2d-46.740296937965546!3d-23.281394987398436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cee6d7af7db769%3A0x30c12e5da363ab56!2sViela+do+Parque+VI+-+Belem+Capela%2C+Francisco+Morato+-+SP%2C+07990-060!5e0!3m2!1spt-BR!2sbr!4v1540414455313" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>	        </div>
      	</div>
      </div>
	  <?php
      include_once("rodape.php");
	  ?>
</body>
</html>		