<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	         <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
</ul>
    <div class="contact">
      	<div class="container">
      	   <div class="register">
		  	  <form action="recebecliente.php" method="post"> 
				 <div class="register-top-grid">
					<h3>INFORMAÇÕES PESSOAIS</h3>
					 <div>
						<span>Nome<label>*</label></span>
						<input name="txtnome" type="text" required id="txtnome"> 
					 </div>
					  <div>
						<span>CPF<label>*</label></span>
						<input name="txtcpf" type="text" required id="txtcpf"> 
					 </div>
					 <div>
						 <span>Email<label>*</label></span>
						 <input name="txtemail" type="text" required id="txtemail"> 
					 </div>
					 <div>
						 <span>Rua<label>*</label></span>
						 <input name="txtrua" type="text" required id="txtrua"> 
					 </div>
					 
					 <div>
						 <span>Número<label>*</label></span>
						 <input name="txtnumero" type="text" required id="txtnumero"> 
					 </div>
					 
					 <div>
						 <span>Cidade<label>*</label></span>
						 <input name="txtcidade" type="text" required id="txtcidade"> 
					 </div>
					 
					  <div>
						 <span>Bairro<label>*</label></span>
						 <input name="txtbairro" type="text" required id="txtbairro"> 
					 </div>
					 
					  <div>
						 <span>Estado<label>*</label></span>
						 <input name="txtestado" type="text" required id="txtestado"> 
					 </div>
					 
					  <div>
						 <span>CEP<label>*</label></span>
						 <input name="txtcep" type="text" required id="txtcep"> 
					 </div>
					 
					  <div>
						 <span>Telefone para Contato<label>*</label></span>
						 <input name="txttelefone" type="text" required id="txttelefone"> 
					 </div>
					 
					 
					 <div class="clearfix"> </div>
					   <a class="news-letter" href="#">
						 <label class="checkbox"><input name="novidade" type="checkbox" checked="" value="1"><i> </i>Receber novidades por email</label>
					   </a>
					 </div>
				     <div class="register-bottom-grid">
						    <h3>INFORMAÇÕES DE ACESSO</h3>
							 <div>
								<span>Senha<label>*</label></span>
								<input name="txtsenha" type="password" required id="txtsenha">
							 </div>
							 <div>
								<span>Confirmar Senha<label>*</label></span>
								<input name="confirmar" type="password" required id="confirmar">
							 </div>
							 <div class="clearfix"> </div>							
					 </div>
				
				<div class="clearfix"> </div>
				<div class="register-but">				
					   <input class="acount-btn" type="submit" value="Enviar">
					   <div class="clearfix"> </div>
				</form>
				</div>
		   </div>
      	 </div>
      </div>
      <div class="grid-2">
       	<?php
      include_once("rodape.php");
	  ?>
</body>
</html>		