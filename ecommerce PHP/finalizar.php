<?php
$total = $_GET['total'];
$duas = 0;
$tres = 0;
$quatro = 0;
$cinco = 0;
$seis = 0;
if($total > 0)
{
	$duas = $total / 2;
	$tres = $total / 3;
	$quatro = $total / 4;
	$cinco = $total / 5;
	$seis = $total / 6;
}

?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script>
	function Desbloquear(opcao)
	{	
	
	document.getElementById("numero").value = "";
			 document.getElementById("nome").value = "";
			 document.getElementById("mes").value = "";
			 document.getElementById("ano").value = "";
			 document.getElementById("vezes").value = "";
			 document.getElementById("digito").value = "";
	
		if(opcao == "boleto")
		{
			 document.getElementById("numero").disabled = true;
			 document.getElementById("nome").disabled = true;
			 document.getElementById("mes").disabled = true;
			 document.getElementById("ano").disabled = true;
			 document.getElementById("vezes").disabled = true;
			 document.getElementById("digito").disabled = true;
		}
		 else
		 {
			  document.getElementById("numero").disabled = false;
			 document.getElementById("nome").disabled = false;
			 document.getElementById("mes").disabled = false;
			 document.getElementById("ano").disabled = false;
			 document.getElementById("vezes").disabled = false;
			 document.getElementById("digito").disabled = false;
		 }
	}
	
	function SomenteNumeros(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}
</script>
</head>
<body onload="Desbloquear('boleto');">
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");
		
		include_once("conectar.php");

		if(!isset($_SESSION['codcli_session']))
			header("location:login.php");
		
		$email = $_SESSION['cliente_session'];
		//buscar endereço do cliente para a entrega
		$consulta = mysql_query("select * from cliente where email = '$email'") or die(mysql_error());
		$dados = mysql_fetch_assoc($consulta);				
		$rua = $dados['rua'];
		$bairro = $dados['bairro'];
		$cidade = $dados['cidade'];
		$estado = $dados['estado'];
		$cep = $dados['cep'];
		$numero = $dados['numero'];		

	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	        <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
</ul>
      <div class="contact">	  
      	<div class="container">
      	   <div class="contact_top">
      		<div class="col-md-3 contact_left">
      			<div>
					<h3>Entrega</h3>	
					<p><?php echo "Rua $rua, n. $numero";?></p>
					<p><?php echo "$cep - $bairro";?></p>
					<p><?php echo "$cidade / $estado";?></p>
				</div>
      		</div>
			 <form method="post" action="finalizarcompra.php">
      		<div class="col-md-9">
      			<div class="contact-form">				  
				   <h3>Pagamento</h3>
					<h2><strong>Total: R$ <?php echo number_format($total,2,",",".");?></strong></h2>				   
					   <div class="form-group">
                                            <label class="radio-inline">
                                                <input type="radio" name="pagto" id="pagto" value="master" onclick="Desbloquear('cartao');" ><img src="images/master.png" class="img-responsive" alt="" align="middle" />
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="pagto" id="pagto" value="visa" onclick="Desbloquear('cartao');"><img src="images/visa.png" class="img-responsive" alt="" align="middle" />
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="pagto" id="pagto" value="boleto" checked onclick="Desbloquear('boleto');">Boleto Bancário
                                            </label>
                        </div>
						
						<div class="form-group">
                                            <label>Número do cartão</label>
                                            <input class="form-control" name="numero" id="numero" onkeypress="SomenteNumeros();">
                        </div>
						
						<div class="form-group">
                                            <label>Nome impresso no cartão</label>
                                            <input class="form-control" name="nome" id="nome">
                        </div>
						
						<div class="form-inline">
							<label>Mês</label>
							<select class="form-control" name="mes" id="mes">
                                                <option>01</option>
												<option>02</option>
												<option>03</option>
												<option>04</option>
												<option>05</option>
												<option>06</option>
												<option>07</option>
												<option>08</option>
												<option>09</option>
												<option>10</option>
												<option>11</option>
												<option>13</option>
                                            </select>
							<label>Ano</label>
							<select class="form-control" name="ano" id="ano">
                                                <option><?php echo date('Y');?></option>
												<option><?php echo date('Y')+1;?></option>                                              
												<option><?php echo date('Y')+2;?></option>
												<option><?php echo date('Y')+3;?></option>
												<option><?php echo date('Y')+4;?></option>
												<option><?php echo date('Y')+5;?></option>
                                            </select>					
                        </div>
						
						<div class="form-group">
                                            <label>Dígito de verificação</label>
                                            <input class="form-control" name="digito" id="digito" maxlength="3" onkeypress="return SomenteNumeros(event);">
                        </div>
						
						  <div class="form-group">
                                            <label>Forma de Pagamento</label>
                                            <select class="form-control" name="vezes" id="vezes">
                                                <option>À Vista - R$ <?php echo number_format($total,2,",",".");?></option>
                                                <option>Parcelado 2x R$ <?php echo number_format($duas,2,",",".");?></option>
                                                <option>Parcelado 3x R$ <?php echo number_format($tres,2,",",".");?></option>
                                                <option>Parcelado 4x R$ <?php echo number_format($quatro,2,",",".");?></option>
                                                <option>Parcelado 5x R$ <?php echo number_format($quatro,2,",",".");?></option>
												<option>Parcelado 6x R$ <?php echo number_format($seis,2,",",".");?></option>
                                            </select>
						</div>
						
						<input type="submit" value="Finalizar Compra"  class="btn1 btn-primary1" />
				 
				</div>
      		</div>
			  </form>
      		<div class="clearfix"> </div>
      	   </div>      		
      	</div>
      </div>
	  <?php
      include_once("rodape.php");
	  ?>
</body>
</html>		