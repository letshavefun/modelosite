<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
</head>
<body>
<div class="header">
   <div class="header_top">
    <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	        <?php 
			include_once("menu.php");
			?>		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
</ul>
      <div class="contact">
      	<div class="container">
      	   <div class="contact_top">      		
      		<div>
      			<div class="contact-form">
				<h2>Carrinho de Compras</h2>

				<?php			
				
				include_once("conectar.php");//conectar no banco de dados
				if(!isset($_SESSION['carrinho']))
				{
					//criando o vetor, caso ele não exista
					$_SESSION['carrinho'] = array();//código dos produtos
					$_SESSION['quantidade'] = array();//quantidade dos produtos
					$_SESSION['cor'] = array();//cor dos produtos
					$_SESSION['tamanho'] = array();//tamanho dos produtos
					$_SESSION['total'] = 0;//para mostrar o valor total
				}				
				
				//pegara a informação do método get para saber a ação a ser realizada
				$acao = "";
				if(!empty($_GET['acao']))
					$acao = $_GET['acao'];
				
				
				if($acao == "add")//verifica se a ação é adicionar
				{					
					$codroupa = $_GET['codroupa'];//pegando o codroupa do get_browser
					if(!in_array($codroupa,$_SESSION['carrinho']))//verificar se o ítem já não foi adicionado
					{
						$_SESSION['carrinho'][$codroupa] = $codroupa;//armazenando este código na sessão
						$_SESSION['quantidade'][$codroupa] = 1;//armazenando a quantidade na sessão
						$_SESSION['cor'][$codroupa] = $_POST['nomecor'];//armazenando a quantidade na sessão
						$_SESSION['tamanho'][$codroupa] = $_POST['tamanho'];//armazenando a quantidade na sessão					
						
						//todos mantem o mesmo código, pois refere-se ao mesmo produto					
					}
				}
				else if($acao == "excluir")	//verificar se a ação é excluir		
				{
					//tirando da sessão de acordo com o código que virá do GET (url)
					$codigo = $_GET['codroupa'];
					unset($_SESSION['carrinho'][$codigo]);
					unset($_SESSION['quantidade'][$codigo]);
					unset($_SESSION['cor'][$codigo]);
					unset($_SESSION['tamanho'][$codigo]);
				}
				else if($acao == "alterar")//verificar se ação é alterar
				{
					//primeiro é verificado se a quantidade é maior que zero
					if($_POST['txtqtd'] > 0){
					$codroupa = $_GET['codroupa'];//pegando o codroupa do get_browser
					$_SESSION['quantidade'][$codroupa] = $_POST['txtqtd'];//armazenando a quantidade na sessão
					}
				}
				
				
				?>
				
							
				
			      <table width="200" border="1" class="table table-hover">
				     <tr>
				       <td><strong>Produto</strong></td>
				       <td><strong>Preço</strong></td>
				       <td><strong>Quantidade</strong></td>
					   <td><strong>Subtotal</strong></td>
					   <td><strong>Excluir</strong></td>
			        </tr>
					<?php 
					$total = 0;//
					foreach($_SESSION['carrinho'] as $prod => $codigo)
					{
						$consulta = mysql_query("select * from roupa where codroupa = '$codigo'") or die (mysql_error());
						$dados = mysql_fetch_assoc($consulta);
						$titulo = $dados['titulo'];
						$valor = $dados['valor'];						
						
						//buscando o nome do tamanho
						$codtamanho = $_SESSION['tamanho'][$codigo];
						$consultatamanho = mysql_query("select * from tamanho where codtamanho = '$codtamanho'") or die (mysql_error());
						$dadostamamho = mysql_fetch_assoc($consultatamanho);						
						$tamanho = $dadostamamho['descricao'];
						
						//buscando o nome da cor
						$codcor = $_SESSION['cor'][$codigo];
						$consultacor = mysql_query("select * from cor where codcor = '$codcor'") or die (mysql_error());
						$dadoscor = mysql_fetch_assoc($consultacor);						
						$cor = $dadoscor['nomecor'];
						
						$qtd = $_SESSION['quantidade'][$codigo];
						$titulo .= ", $tamanho, $cor";
						$subtotal = $valor * $qtd;					
						$total += $subtotal;
					
					?>
					
				     <tr>
				       <td><?php echo $titulo;?></td>
				       <td><?php echo 'R$ '.number_format($valor,2,",","."); ?></td>
				       <td>
					   
					   <form action="carrinho.php?acao=alterar&codroupa=<?php echo $codigo;?>" method="post">
					   <input name="txtqtd" type="number" id="txtqtd" value="<?php echo $qtd;?>" size="1">					   
					   <input type=image src="adm/pages/imagens/check.png" />
					   </form>	
					   
					   </td>
					   <td><?php echo 'R$ '.number_format($subtotal,2,",",".");?></td>
					   <td><a href="carrinho.php?acao=excluir&codroupa=<?php echo $codigo;?>"><img src="adm/pages/imagens/excluir.png" alt=""> Excluir  </a></td>					   
			        </tr>
					<?php 
					}
					?>
			      </table>
				  <h2><strong>Total: R$ <?php echo number_format($total,2,",",".");?></strong></h2>
				  <?php 
						$_SESSION['total'] = $total;//manda para a sessão o total para exibir no carrinho					   
						$link = "";
						if(count($_SESSION['carrinho']) > 0) 							
						{			
							$link = "finalizar.php?total=$total";
						}
						else
						{
							$link = "#";
						}
					
				  ?>
				  <p><a href="<?php echo $link;?>" class="btn1 btn-primary1"><span>Finalizar Compra</span></a>
				  <a href="index.php" class="btn1 btn-primary1"><span>Continuar comprando</span></a><p>
                </div>
      		</div>
      		<div class="clearfix"> </div>
      	   </div>
           </div>
      </div>
	  <?php
      include_once("rodape.php");
	  ?>
</body>
</html>		