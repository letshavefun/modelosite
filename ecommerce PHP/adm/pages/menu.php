<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="index.php"><img src="imagens\home.png"> Início</a>
                        </li>
						<li>
                            <a href="#"><img src="imagens\compras.png"> Compras</a>
                        </li>
						<li>
                            <a href="#"><img src="imagens\roupa.png">Cadastro de Roupas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="roupa.php?tipo=m">Roupas Masculinas</a>
                                </li>	
                                <li>
                                    <a href="roupa.php?tipo=f">Roupas Femininas</a>
                                </li> 							
                            </ul>
                            <!-- /.nav-second-level -->
                        </li> 
						<li>
                            <a href="consultaroupas.php"><img src="imagens\roupa.png"> Consulta de Roupas</a>
                        </li>
 						
						<li>
                            <a href="consultacliente.php"><img src="imagens\usuario.png"> Clientes</a>
                        </li>
                        <li>
                            <a href="consultacontato.php"><img src="imagens\contato.png"> Contato</a>
                        </li>
                        <li>
                            <a href="#"><img src="imagens\config.png"></i>Configurações<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="usuario.php">Usuários do Sistema</a>
                                </li>	
                                <li>
                                    <a href="categoria.php">Cadastro de Categorias</a>
                                </li> 							
								<li>
                                    <a href="tamanho.php">Cadastro de Tamanhos</a>
                                </li>						
								<li>
                                    <a href="cor.php">Cadastro de Cores</a>
                                </li>							
								<li>
                                    <a href="config/sair.php">Sair</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>                  
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>