<?php 
include_once("conectar.php");//incluir arq. conectar
include_once("verifica.php");//incluir verificação
$acao = "";
if(!empty($_GET["acao"])) //pega a ação para saber se é alteração, a fim de preencher os campos do formulário
	$acao = $_GET["acao"];

//variáveis para preencher o formulário
$nomecateg = "";
$codcateg = "";
	
if($acao == "alterar")//se a ação for alterar
{
	$codcateg = $_GET['codcateg'];//pega o código através do GET (url)
	
  //comando SQL para consulta de acordo com um codigo específico
  $busca = mysql_query("select * from categoria where codcateg = '$codcateg'") or die (mysql_error());
  
  $dados = mysql_fetch_assoc($busca);
  
  //variaveis necessárias (ver banco de dados)
  $nomecateg = $dados['nomecateg'];    
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema Administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include_once("topo.php");?>
            <!-- /.navbar-top-links -->
             <?php include_once("menu.php");?>
            <!-- /.navbar-static-side -->
        </nav>

      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastro de Categorias</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Dados da Categoria
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            	<form action="recebecategoria.php" method="post" role="form">						
							    <div class="form-group">
                                            <label>Nome da categoria</label>
                                            <input name="txtcategoria" class="form-control" id="txtcategoria" placeholder="Informe o nome da categoria" value="<?php echo $nomecateg;?>" required>                                           
                                </div>								

								<input type="hidden" name="codcateg" id="codcateg" value="<?php echo $codcateg;?>"/>
								<input type="hidden" name="acao" id="acao" value="<?php echo $acao;?>"/>
						    <button type="submit" class="btn btn-primary">Gravar</button>
							<a href="categoria.php"><button type="button" class="btn btn-primary">Novo</button></a>
						   </form>
                        </div>						
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
			
			
            <!-- /.row -->
            <div class="row">
                             <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Consulta de Categorias
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
											<th>Categoria</th>
                                            <th>Ação</th>
                                        </tr>
                                    </thead>									
                                    <tbody>	
									<?php
									  
									  //comando SQL para consulta
									  $busca = mysql_query("select * from categoria") or die (mysql_error());
									  
									  while($dados = mysql_fetch_assoc($busca))
									  {
										  //variaveis necessárias (ver banco de dados)
										  $nomecateg = $dados['nomecateg'];							  
										  $codcateg = $dados['codcateg'];
										  
									  
									  ?>  
									
                                        <tr>
											<td><?php echo $nomecateg;?></td>
                                            <td> 
											<a href="categoria.php?acao=alterar&codcateg=<?php echo $codcateg;?>"><button type="button" class="btn btn-info btn-circle" title="Editar"><img src="imagens\editar.png">
                            </button></a>
											
											<a href="recebecategoria.php?acao=excluir&codcateg=<?php echo $codcateg;?>" onclick="return confirm('Confirma exclusão desta categoria?')"><button type="button" class="btn btn-warning btn-circle" title="Excluir"><img src="imagens\excluir.png">
                            </button></a></td>
                                        </tr>                                       
                                    </tbody>
									  <?php } ?>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
        </div>
          <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
