<?php 
include_once("conectar.php");//incluir arq. conectar
include_once("verifica.php");//incluir verificação
$acao = "";
if(!empty($_GET["acao"])) //pega a ação para saber se é alteração, a fim de preencher os campos do formulário
	$acao = $_GET["acao"];

//variáveis para preencher o formulário
$descricao = "";
$codtamanho = "";
	
if($acao == "alterar")//se a ação for alterar
{
	$codtamanho = $_GET['codtamanho'];//pega o código através do GET (url)
	
  //comando SQL para consulta de atamanhodo com um codigo específico
  $busca = mysql_query("select * from tamanho where codtamanho = '$codtamanho'") or die (mysql_error());
  
  $dados = mysql_fetch_assoc($busca);
  
  //variaveis necessárias (ver banco de dados)
  $descricao = $dados['descricao'];    
}

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema Administrativo</title>

    <!-- Bootstrap tamanhoe CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include_once("topo.php");?>
            <!-- /.navbar-top-links -->
             <?php include_once("menu.php");?>
            <!-- /.navbar-static-side -->
        </nav>

      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastro de Tamanhos</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Dados do tamanho
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            	<form action="recebetamanho.php" method="post" role="form">						
							    <div class="form-group">
                                            <label>Nome do tamanho</label>
                                            <input name="txttamanho" class="form-control" id="txttamanho" placeholder="Informe a descrição tamanho" value="<?php echo $descricao;?>" required>                                           
                                </div>								

								<input type="hidden" name="codtamanho" id="codtamanho" value="<?php echo $codtamanho;?>"/>
								<input type="hidden" name="acao" id="acao" value="<?php echo $acao;?>"/>
						    <button type="submit" class="btn btn-primary">Gravar</button>
							<a href="tamanho.php"><button type="button" class="btn btn-primary">Novo</button></a>
						   </form>
                        </div>						
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
			
			
            <!-- /.row -->
            <div class="row">
                             <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Consulta de tamanhoes
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
											<th>Descrição</th>
                                            <th>Ação</th>
                                        </tr>
                                    </thead>									
                                    <tbody>	
									<?php
									  
									  //comando SQL para consulta
									  $busca = mysql_query("select * from tamanho") or die (mysql_error());
									  
									  while($dados = mysql_fetch_assoc($busca))
									  {
										  //variaveis necessárias (ver banco de dados)
										  $descricao = $dados['descricao'];							  
										  $codtamanho = $dados['codtamanho'];
										  
									  
									  ?>  
									
                                        <tr>
											<td><?php echo $descricao;?></td>
                                            <td> 
											<a href="tamanho.php?acao=alterar&codtamanho=<?php echo $codtamanho;?>"><button type="button" class="btn btn-info btn-circle" title="Editar"><img src="imagens\editar.png">
                            </button></a>
											
											<a href="recebetamanho.php?acao=excluir&codtamanho=<?php echo $codtamanho;?>" onclick="return confirm('Confirma exclusão desta tamanho?')"><button type="button" class="btn btn-warning btn-circle" title="Excluir"><img src="imagens\excluir.png">
                            </button></a></td>
                                        </tr>                                       
                                    </tbody>
									  <?php } ?>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
        </div>
          <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap tamanhoe JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
