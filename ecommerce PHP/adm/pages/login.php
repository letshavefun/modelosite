<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema Administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">			
                <div class="login-panel panel panel-primary">				
                    <div class="panel-heading">				
                        <h3 class="panel-title">Sistema Administrativo</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuário" name="txtusuario" id = "txtusuario"  type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="txtsenha" id = "txtsenha" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
									
                                        <a href="#" data-toggle="modal" data-target="#exampleModal">Recuperar senha</a>					
										<!-- MODAL -->
										<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="exampleModalLabel">Recuperação de senha</h4>
											  </div>
											  <div class="modal-body">
												<form>
												  <div class="form-group">
													<label for="email" class="control-label">E-mail de recuperação:</label>
													<input type="email" class="form-control" id="email" name="email">
												  </div>												 
												</form>
											  </div>
											  <div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
												<button type="button" class="btn btn-primary">Enviar</button>
											  </div>
											</div>
										  </div>
										</div>
										
										
										<!-- FIM MODAL -->
										
										
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-primary btn-block" value="Entrar">
                            </fieldset>
							<?php		
		if(!empty($_POST))//verifica se todos os campos foram preenchidos
		{
			include_once("conectar.php");//incluir conexão
			
			$usu = $_POST['txtusuario'];//pega usuário do formulário
			$senha = $_POST['txtsenha'];//pega senha do formulário
			
			
			//$senha = md5($senha);//converte a senha para criptografia MD5
			
			
			$verifica = mysql_query("SELECT * FROM usuario WHERE nomeusuario = '$usu' AND senha = '$senha'") or die(mysql_error());
			
			$retorno = mysql_fetch_array($verifica);//pega a quantidade de linhas retornadas
			
			 if (mysql_num_rows($verifica)<=0) // caso o numero de linhas seja menor ou igual a 0
			 {
				 //informa a mensagem de erro
				echo "<br><div class='alert alert-danger' role='alert'>
					  <span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
					  <span class='sr-only'>Error:</span>
					  Usuário e/ou senha incorretos!
					</div>";
			 }
			 else//caso contrário
			 {
				session_start();//inicia a sessão
				if(!isset($_SESSION['usuario_session']))//se não existe uma sessão criada
				{
					$_SESSION['usuario_session'] = $usu;//cria a sessão e armazena o nome do usuário na sessão					
				}
				header("Location:index.php");//redireciona para principal
			 }
			mysql_close($conectar);//desconecta do banco de dados
		}
		
		?>			
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script>
	$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-title').text('Recuperar Senha')
  modal.find('.modal-body input').val(recipient)
})
	</script>

</body>

</html>
