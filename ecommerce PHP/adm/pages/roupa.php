<?php 
include_once("verifica.php");
include_once("conectar.php");
$tipo = $_GET['tipo'];
$codroupa = "";
$acao="";
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema Administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	
	<!-- 1º Adicionamos o arquivo CSS do plugin ao código. -->
      <!-- Datepicker -->
      <link href="../dist/css/datepicker.css" rel="stylesheet">   
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include_once("topo.php");?>
            <!-- menus -->
             <?php include_once("menu.php");?>
            <!-- fim menus -->
        </nav>

      <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Cadastro de Roupas <?php if($tipo == "f")echo "Femininas"; else echo "Mascuinas";?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Dados da roupa
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            	<form action="receberoupa.php" method="post" role="form" enctype="multipart/form-data">						
							    <div class="form-group">
                                            <label>Título</label>
                                            <input name="txttitulo" class="form-control" id="txttitulo" placeholder="Informe título da roupa" required value="">                                           
                                </div>
								
								 <div class="form-group">
                                            <label>Valor</label>
                                            <input name="txtvalor" class="form-control" id="txtvalor" placeholder="Informe o valor da roupa" required value="">                                           
                                </div>
								
								 <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input name="novidade" type="checkbox" id="novidade" value="1" >Marcar como Novidade
                                            </label>                                            
                                  </div>
								  
								  <div class="form-group">
                                            <label class="checkbox-inline">
                                                <input name="off" type="checkbox" id="off" value="1" >Marcar como 50% off
                                            </label>                                            
                                  </div>				 
								
								<div class="form-group">
                                            <label>Categoria</label>
                                            <select name="categoria" class="form-control" id="categoria">
											<?php 
											$buscacateg = mysql_query("select * from categoria order by nomecateg") or die (mysql_error());
											while($linha = mysql_fetch_assoc($buscacateg))
											{
												$codcateg = $linha['codcateg'];
												$nomecateg = $linha['nomecateg'];
											?>
											
                                              <option value="<?php echo $codcateg;?>"><?php echo $nomecateg;?></option>                  
											<?php }?>
                                            </select>
                                </div>															
								 
								<div class="form-group">
                                            <label>Foto 1</label>
                                            <input type="file" name="foto1">
                                        </div>
										<div class="form-group">
                                            <label>Foto 2</label>
                                            <input type="file" name="foto2">
                                        </div>									
								
								  
								   <div class="form-group">
                                            <label>Descrição da roupa</label>
                                            <textarea name="txtdescricao" rows="3" class="form-control" id="txtdescricao"></textarea>
                                        </div>
                           
						   <input type="hidden" name="tipo" id="tipo" value="<?php echo $tipo;?>"/>
						   <input type="hidden" name="codroupa" id="codroupa" value="<?php echo $codroupa;?>"/>
						   <input type="hidden" name="acao" id="acao" value="<?php echo $acao;?>"/>
						   
						    <button type="submit" class="btn btn-primary">Gravar</button>														
                            <button type="reset" class="btn btn-primary">Limpar Campos</button>														
				           </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
          </div>
            <!-- /.row -->
            <div class="row">
              <!-- /.col-lg-6 -->
              <!-- /.col-lg-6 -->
        </div>
          <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- JQuery data -->
<link rel="stylesheet" href="../js/jquery-ui.css" />
<script src="../js/jquery-1.8.2.js"></script>
<script src="../js/jquery-ui.js"></script>
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
	
	
	 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   
   <!--<script src="js/jquery.min.js"></script>-->

    <!-- Referência do arquivo JS do plugin após carregar o jquery -->
      <!-- Datepicker -->
      <script src="js/bootstrap-datepicker.js"></script>
	  <script src="js/brasil.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->

  <script>
      $(document).ready(function () {
        $('#data').datepicker({
			language: "pt-BR",
            format: "dd/mm/yyyy",
			todayHighlight: true            
        });
      });
  </script>
	
	
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>

