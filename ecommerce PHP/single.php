<?php 
include_once("conectar.php");
$codroupa = "";
if(!empty($_GET["codroupa"]))
	$codroupa = $_GET["codroupa"];
$buscar = mysql_query("select * from roupa where codroupa = '$codroupa'") or die (mysql_error());
$dados = mysql_fetch_array($buscar);
$titulo = $dados['titulo'];
$valor = number_format($dados["valor"],2,",",".");
$descricao = $dados['descricao'];
$foto1 = $dados['foto1'];
$foto2 = $dados['foto2'];

?>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lookshop - Vendas</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<link rel="stylesheet" href="css/etalage.css">
<script src="js/jquery.etalage.min.js"></script>
<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>
<!--initiate accordion-->
<script type="text/javascript">
	$(function() {
	
	    var menu_ul = $('.menu > li > ul'),
	           menu_a  = $('.menu > li > a');
	    
	    menu_ul.hide();
	
	    menu_a.click(function(e) {
	        e.preventDefault();
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	
	});
	
</script>
</head>
<body>
<div class="header">
  <div class="header_top">
    <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	         <?php 
			include_once("menu.php");
			?>
		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<ul class="breadcrumbs">
 <div class="container">
</ul>
<div class="single_top">
	 <div class="container"> 
	      <div class="single_grid">
				<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a href="optionallink.html">
									<img class="etalage_thumb_image" src="adm/pages/<?php echo $foto1;?>" class="img-responsive" />
									<img class="etalage_source_image" src="adm/pages/<?php echo $foto1;?>" class="img-responsive" title="" />
								</a>
							</li>
							<li>
								<img class="etalage_thumb_image" src="adm/pages/<?php echo $foto2;?>" class="img-responsive" />
								<img class="etalage_source_image" src="adm/pages/<?php echo $foto2;?>" class="img-responsive" title="" />
							</li>
							
						</ul>
						 <div class="clearfix"></div>		
				  </div> 
				  <div class="desc1 span_3_of_2">
				  	<ul class="back">
                	  <li><i class="back_arrow"> </i>Voltar para <a href="index.php">Home</a></li>
                    </ul>
					<h1><?php echo $titulo;?></h1>
					<ul class="price_single">
					  <li class="head"><h2>R$ <?php echo $valor;?></h2></li>
					  
					  <div class="clearfix"></div>
					</ul>
					<p><?php echo $descricao;?></p>
				     
					 <form action="carrinho.php?codroupa=<?php echo $codroupa;?>&acao=add" method="post">
					 
					 <div class="dropdown_top">					 
				       <div class="dropdown_left">   
					     <select name="tamanho" id="tamanho" class="dropdown" tabindex="10" data-settings='{"wrapperClass":"metro1"}' required>
	            			<option value="">Selecione o Tamanho</option>	
							<?php 
							$buscar = mysql_query("select * from tamanho") or die (mysql_error());
							while($dados = mysql_fetch_assoc($buscar))
							{
								$codtamanho = $dados['codtamanho'];
								$descricao  = $dados['descricao'];
							
							?>
							
							<option value="<?php echo $codtamanho;?>"><?php echo $descricao;?></option>
							<?php
							}
							?>
			             </select>
			            </div>
			           <div class="dropdown_left">
					     <select name="nomecor" id="nomecor" class="dropdown" tabindex="10" data-settings='{"wrapperClass":"metro1"}' required>
	            			<option value="">Selecione a Cor</option>	
							<?php 
							$buscar = mysql_query("select * from cor") or die (mysql_error());
							while($dados = mysql_fetch_assoc($buscar))
							{
								$codcor = $dados['codcor'];
								$nomecor  = $dados['nomecor'];
							
							?>
							<option value="<?php echo $codcor;?>"><?php echo $nomecor;?></option>
							<?php 
							}
							?>
			             </select>
			            </div>
						 <div class="clearfix"></div>
			         </div>
					 <input type="submit" value="ADICIONAR AO CARRINHO"  class="btn1 btn2 btn-primary1"/>			         
					
				</div>
          	    <div class="clearfix"></div>
          	   </div>   

 </form>			   
   </div>
      </div>
      <div class="grid-2">
       	<?php
      include_once("rodape.php");
	  ?>
</body>
</html>		