-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14-Nov-2015 às 00:38
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bdloja`
--
-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `codcateg` int(11) NOT NULL AUTO_INCREMENT,
  `nomecateg` varchar(50) NOT NULL,
  PRIMARY KEY (`codcateg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`codcateg`, `nomecateg`) VALUES
(3, 'Inverno'),
(5, 'Nova categoria'),
(6, 'VerÃ£o');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `codcli` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `cpf` varchar(14) NOT NULL,
  `email` varchar(70) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cep` varchar(11) NOT NULL,
  `telefone` varchar(14) NOT NULL,
  `novidade` tinyint(1) NOT NULL,
  `senha` varchar(70) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`codcli`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`codcli`, `nome`, `cpf`, `email`, `rua`, `numero`, `cidade`, `bairro`, `estado`, `cep`, `telefone`, `novidade`, `senha`, `ativo`) VALUES
(9, 'Anderson Alves', '445.451.128.46, 'anderson.a.s.m@hotmail.com', 'teste', 'teste', 'dsaf', 'fdas', 'sp', '1234', '4444-5555', 1, 'admin', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `codcontato` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(70) NOT NULL,
  `mensagem` text NOT NULL,
  `data` date NOT NULL,
  `visualizado` tinyint(1) NOT NULL,
  PRIMARY KEY (`codcontato`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`codcontato`, `nome`, `email`, `mensagem`, `data`, `visualizado`) VALUES
(1, 'ALVES', 'alves@mail.com', 'ola mundo', '2015-09-19', 1),
(2, 'Testando ', 'testando@gmail.com', 'NÃ£o gostei do site, achei muito difÃ­cil', '2015-09-19', 1),
(3, 'Anderson', 'anderson@gmail.com', 'teste de mensagem', '2015-09-19', 1),
(4, 'Anderson', 'anderson@gmail.com', 'dsgf', '2015-10-23', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cor`
--

CREATE TABLE IF NOT EXISTS `cor` (
  `codcor` int(11) NOT NULL AUTO_INCREMENT,
  `nomecor` varchar(30) NOT NULL,
  PRIMARY KEY (`codcor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `cor`
--

INSERT INTO `cor` (`codcor`, `nomecor`) VALUES
(2, 'Azul'),
(3, 'vermelho'),
(4, 'Preto'),
(5, 'Amarelo'),
(6, 'Listrado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens`
--

CREATE TABLE IF NOT EXISTS `itens` (
  `coditem` int(11) NOT NULL AUTO_INCREMENT,
  `codroupa` int(11) NOT NULL,
  `codpedido` int(11) NOT NULL,
  `qtdparcial` int(11) NOT NULL,
  `tamanho` varchar(5) NOT NULL,
  `cor` varchar(20) NOT NULL,
  PRIMARY KEY (`coditem`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `itens`
--

INSERT INTO `itens` (`coditem`, `codroupa`, `codpedido`, `qtdparcial`, `tamanho`, `cor`) VALUES
(1, 3, 1, 9, '2', '2'),
(2, 4, 1, 4, '3', '3'),
(3, 7, 2, 1, '5', '3'),
(4, 7, 3, 1, '4', '4');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
  `codpedido` int(11) NOT NULL AUTO_INCREMENT,
  `codcli` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `formapagto` varchar(30) NOT NULL,
  `ncartao` varchar(30) NOT NULL,
  `digito` varchar(3) NOT NULL,
  `mes` varchar(2) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `vezes` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`codpedido`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `pedido`
--

INSERT INTO `pedido` (`codpedido`, `codcli`, `data`, `total`, `formapagto`, `ncartao`, `digito`, `mes`, `ano`, `vezes`, `status`) VALUES
(1, 9, '2015-11-13 22:54:00', '2300.00', 'master', '123412341324', '123', '11', '2020', 'Parcelado 6x R$ 383,33', 'aguardando'),
(2, 9, '2015-11-13 23:00:00', '18.00', 'boleto', '', '', '', '', '', 'aguardando'),
(3, 9, '2015-11-14 00:38:00', '18.00', 'master', '12345678', '324', '01', '2016', 'Parcelado 3x R$ 6,00', 'aguardando');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roupa`
--

CREATE TABLE IF NOT EXISTS `roupa` (
  `codroupa` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `descricao` text NOT NULL,
  `codcateg` int(11) NOT NULL,
  `foto1` varchar(100) NOT NULL,
  `foto2` varchar(100) NOT NULL,
  `novidade` tinyint(1) NOT NULL,
  `off` tinyint(1) NOT NULL,
  `tipo` char(1) NOT NULL,
  PRIMARY KEY (`codroupa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `roupa`
--

INSERT INTO `roupa` (`codroupa`, `titulo`, `valor`, `descricao`, `codcateg`, `foto1`, `foto2`, `novidade`, `off`, `tipo`) VALUES
(3, 'teste', '100.00', 'Roupa blalbalbl', 6, 'images/deae21afa4c8cea086b03abb9ac7d026.jpg', 'images/0d56bbd3fe25f33db6077d4e71b3227e.jpg', 1, 0, 'f'),
(4, 'teste', '350.00', 'teste de descriÃ§Ã£o', 3, 'images/bb27b5dbf48dd570507846fc03bd5576.jpg', 'images/06a351e5e7e0287d26bc17abf2a02577.jpg', 1, 1, 'f'),
(5, 'roupa feminina', '99.00', 'asdasdfasfdasfadfsdafsd', 3, 'images/8665f98943684f6ecb3c07c03a2e77f0.jpg', 'images/27a8d5127d1b4563cc09533f7e1e42ce.jpg', 1, 0, 'f'),
(6, 'roupa mascuina', '55.00', 'sad', 6, 'images/a93e0e5f53ff30931bfb5c00d7d48388.jpg', 'images/c7badd3b5ae6f0e65598adcaadc8b06a.jpg', 0, 0, 'm'),
(7, 'teste de roupa', '18.00', 'aaaaa', 6, 'images/bfc0ddd67d8ad638f777306631025541.jpg', 'images/8cc1b2579053c5bc6e6c115f29cd0ff4.jpg', 0, 1, 'm');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roupacor`
--

CREATE TABLE IF NOT EXISTS `roupacor` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codcor` int(11) NOT NULL,
  `codroupa` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `codcor` (`codcor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `roupatamanho`
--

CREATE TABLE IF NOT EXISTS `roupatamanho` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codtamanho` int(11) NOT NULL,
  `codroupa` int(11) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tamanho`
--

CREATE TABLE IF NOT EXISTS `tamanho` (
  `codtamanho` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`codtamanho`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `tamanho`
--

INSERT INTO `tamanho` (`codtamanho`, `descricao`) VALUES
(2, 'P'),
(3, 'M'),
(4, 'PP'),
(5, 'G'),
(6, 'GG');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `codusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nomeusuario` varchar(50) NOT NULL,
  `senha` varchar(70) NOT NULL,
  `emailusuario` varchar(70) NOT NULL,
  PRIMARY KEY (`codusuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`codusuario`, `nomeusuario`, `senha`, `emailusuario`) VALUES
(8, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `roupacor`
--
ALTER TABLE `roupacor`
  ADD CONSTRAINT `roupacor_ibfk_1` FOREIGN KEY (`codcor`) REFERENCES `cor` (`codcor`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
