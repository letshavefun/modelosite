<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Lookshop - Vendas</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Oxygen:300,400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/responsiveslides.min.js"></script>
<script>
    $(function () {
      $("#slider").responsiveSlides({
      	auto: true,
      	nav: true,
      	speed: 500,
        namespace: "callbacks",
        pager: true,
      });
    });
</script>
<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
		    <script type="text/javascript">
			    $(document).ready(function () {
			        $('#horizontalTab').easyResponsiveTabs({
			            type: 'default', //Types: default, vertical, accordion           
			            width: 'auto', //auto or any width like 600px
			            fit: true   // 100% fit in a container
			        });
			    });
</script>	
</head>
<body>
<div class="header">
   <div class="header_top">
   <?php
		include_once("topo.php");
	?>
  </div>
  <div class="header_bottom">
	<div class="container">	 			
		<div class="logo">
		  <a href="index.html"><img src="images/logo.png" alt=""/></a>
		</div>	
		<div class="header_bottom_right">			
	        <?php 
			include_once("menu.php");
			?>
		  
	      <div class="clearfix"></div>		   
      </div>
    </div>
  </div>
</div>
<div class="slider">
	  <div class="callbacks_container">
	      <ul class="rslides" id="slider">
	        <li><img src="images/banner.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="images/banner1.jpg" class="img-responsive" alt=""/></li>
	        <li><img src="images/banner4.jpg" class="img-responsive" alt=""/></li>
	      </ul>
	  </div>
</div>
<div class="column_center">
  <div class="container">
	<div class="search">
	 
	  <div class="clearfix"> </div>
	</div>
	<ul class="social">
	  <li class="find">Redes Sociais</li>
	  <li><a href="#"> <i class="fb"> </i> </a></li>
	  <li><a href="#"> <i class="tw"> </i> </a></li>
	  <li><a href="#"> <i class="dribble"> </i></a></li>
	  <div class="clearfix"> </div>
	</ul>
	<div class="clearfix"> </div>
  </div>
</div>
<div class="brand">
 <div class="container">
	<img src="images/brands.jpg" class="img-responsive" alt=""/>
 </div>
</div>
<div class="sap_tabs">	
						 <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						  <ul class="resp-tabs-list">
						  	  <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Novidades</span></li>
							  <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>50% Off</span></li>
							  <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Masculino</span></li>
							    <li class="resp-tab-item" aria-controls="tab_item-3" role="tab"><span>Feminino</span></li>
							  <div class="clear"></div>
						  </ul>				  	 
							<div class="resp-tabs-container">
							    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
									<ul class="tab_img">
									  <li>
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>								 
									 
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>	
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>	
										
										
									    <li>
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>	
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>	
										
										
										<li class="last">
										<div class="view view-first">
					   		  			   <img src="images/pic1.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										</div>
									 </li>	
									 
									 
										<div class="clearfix"></div>
									</ul>
							     </div>	
								 
								 
							     <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
									<ul class="tab_img">
									  <li>
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="#" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="#" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
									    <li>
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="#" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="#" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li class="last">
										<div class="view view-first">
					   		  			   <img src="images/pic8.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
											<div class="sale-box"> </div>
										</li>
										<div class="clearfix"></div>
									</ul>
							     </div>	
								 
								 
							     <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
									<ul class="tab_img">
									  <li>
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										
										<li>
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										
										<li>
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										
									    <li>
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										
										<li>
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										
										<li class="last">
										 <div class="view view-first">
					   		  			   <img src="images/pic12.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adcionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										  <div class="sale-box"> </div>
										</li>
										
										<div class="clearfix"></div>
									</ul>
							     </div>	
								 
								 
							     <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-3">
									<ul class="tab_img">
									  <li>
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
									   <li>
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li>
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										
										<li class="last">
										<div class="view view-first">
					   		  			   <img src="images/pic10.jpg" class="img-responsive" alt=""/>
											 <div class="mask">
						                        <div class="info"> </div>
								              </div>
								              <div class="tab_desc">
												  <h3><a href="#">Feel Tank</a></h3>
												  <p>R$ 59,95</p>
												  <a href="single.php" class="btn1 btn-primary1"><span>Adicionar</span><img src="images/plus.png" alt=""/></a>
											  </div>
										  </div>
										</li>
										
										<div class="clearfix"></div>
									</ul>
							     </div>		        					 	        					 
						  </div>	
                  </div>
          </div>
                   
       <div class="brands">
       	<div class="m_3"><span class="left_line"> </span><h3>Trabalhamos com as marcas</h3><span class="right_line"> </span></div>
       	<div class="container">
       		<ul class="brands_list">
       		  <li><img src="images/br1.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br2.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br3.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br4.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br5.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br6.jpg" class="img-responsive" alt=""/></li>
       		  <li><img src="images/br7.jpg" class="img-responsive" alt=""/></li>
       		  <li class="brand_last"><img src="images/br8.jpg" class="img-responsive" alt=""/></li>
       		  <div class="clearfix"> </div>
       		</ul>
       	</div>
       </div>
       <div class="grid-1">
       	<div class="container">
       		<h1>Sobre LookShop </h1>
       		<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis</p>
       	</div>
       </div>
       <div class="grid-2">
       	<?php
      include_once("rodape.php");
	  ?>
</body>
</html>		