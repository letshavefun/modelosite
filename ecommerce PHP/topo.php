<?php
session_start();
$usuariologado = "";
$total = 0;
if(isset($_SESSION['cliente_session']))//caso exista
{
	$usuariologado = $_SESSION['cliente_session'];//armazena em uma variável
}
if(isset($_SESSION['total']))//caso exista
{
	$total = $_SESSION['total'];//armazena em uma variável
}
?>

<div class="container">
	  <div class="header_top_left">
	  	<p>Vendas por telefone : (11)3255-4400 <?php if($usuariologado != "") echo "| Usuário logado: $usuariologado";?></p>
	  </div>
	  <div class="header_top_right">
	  	<?php
		if($usuariologado == "")
		{
			
			?>
   		<ul class="header_user_info">
		  <a class="login" href="login.php">
			<i class="user"></i> 
			<li class="user_desc">Login</li>			
		  </a>
		 
	    </ul>
		<?php }
		else
		{
			?>
		<ul class="header_user_info">
		  <a class="" href="sair.php">
			<li class="user_desc">Sair</li>			
		  </a>
		 
	    </ul>
		
		<?php 
		}
		?>
		
		<ul class="header_user_info">
		  <a class="login" href="carrinho.php">
			<li class="user_desc">Meu Carrinho: R$ <?php echo number_format($total,2,",",".");?></li>						
		  </a>
        </ul>
	    <!-- start search-->
			<div class="search-box">			   
			 </div>
			 <!----search-scripts---->
			 <script src="js/classie.js"></script>
			 <script src="js/uisearch.js"></script>
			   <script>
				 new UISearch( document.getElementById( 'sb-search' ) );
			   </script>
				<!----//search-scripts---->
	            <div class="clearfix"> </div>
		 </div>
	  <div class="clearfix"> </div>
	</div>